# .bashrc supplied for Assignment 2
#
# Student name: David Weisiger 
# Student PID: dbw
#
# Student name:  
# Student PID:   
#
# Set alias(es) here:
alias h="history | tail"
alias hg="history | grep"

# I strongly suggest you not modify anything below this comment!
#
# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

