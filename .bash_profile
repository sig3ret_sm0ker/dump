# .bash_profile supplied for Assignment L01
#
# Student name: David Weisiger 
# Student PID: dbw  
#
# Student name:  
# Student PID:   
#
# Get the aliases and functions from local config file:
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi
#
# Set path here:
PATH="~/bin:$PATH:./"
#
# Set prompt here:
LIGHT_GREEN='\033[1;32m'
NC='\033[0m'
export PS1="\# \u@${LIGHT_GREEN}\h${NC} in \W > "
#
# Set default file permissions here:
umask g=,o=
# end .bash_profile
