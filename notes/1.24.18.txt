---piping commands---
this | that, pipes this to that
this > that, outputs this to that [output redirection operator]
this < that, takes input from that to this [input redirection operator]

----chmod----
chmod 123
- first number is for user, second number for group, third number for other
it's in octal, so..., ex. chmod 740 is...
owner: rwx
group: r--
other: ---

---tar---
tar cvf output.tar *.c *.py *.txt [IMPORTANT: don't flip these around, otherwise, it will overrwrite your files!!!]
create archive, verbose means i want to see what's going on, write to file
tar xvf will extract, -C copies to a directory

---scp---
this command will transfer from local to remote securely

& at the end runs process in the background